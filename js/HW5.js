// Переишіть цей код використовуючи let const~
// Замість var
// for (var i = 0; i < 5; i++) {
//     console.log(i);

// for (let i=0; i<5; i++){
//     console.log(i);
// }





// // Замість var
// var message = 'test';
// function example() {
//     if (true) {
//         var message = 'Hello, world!';
//         console.log(message);
//     }
//     console.log(message); // Виведе 'Hello, world!'
// }

// const message = 'test';
// function example(){
//     if (true) {
//         let message = 'Hello, world';
//         console.log(message);
//     }
//     console.log(message);
// }
// example();




// Використовуючи for in  виведіть значення з обєктку в console.log
// const person = {
//     name: 'John',
//     age: 25,
//     occupation: 'Developer'
// };

// const person = {
//     name: 'John',
//     age: 25,
//     occupation: 'Developer'
// };
// for(let Data in person){
//     console.log(`Data ${Data} value ${person[Data]}`);
// }




// Використовуючи for of  та for in  виведіть занчення з обєктів які знаходяться в масиві
// const students = [
//     { name: 'Alice', age: 20, grade: 'A' },
//     { name: 'Bob', age: 22, grade: 'B' },
//     { name: 'Charlie', age: 21, grade: 'C' }
// ];

// const students = [
//         { name: 'Alice', age: 20, grade: 'A' },
//         { name: 'Bob', age: 22, grade: 'B' },
//         { name: 'Charlie', age: 21, grade: 'C' }
//     ];
//     for(let student of students){
//         console.log(student);
//         for(let key in student){
//         console.log(`info${student[key]}`);
//         }   
//     }
    




// Завдання на map

// Завдання 1: Створіть масив чисел і використайте метод map, щоб створити новий масив, який містить квадрати кожного числа.

// let newArr = [25, 58, 64, 129];
// let doubleArr = newArr.map(number=>number*number);
// console.log(doubleArr);

// Завдання 2: Напишіть функцію, яка приймає масив і функцію-перетворювач, і використовує map для створення нового масиву, в якому кожен елемент отримує  стрінг до значення 'new value'

// let firstValue = (a)=>a+'new value';
// let newValue = [1,2,3,4,5].map(firstValue);
// console.log(newValue);




// Напишіть функцію, яка використовує forEach для обчислення суми всіх елементів у масиві чисел. Масив наприклад const numberArr = [10,20,30]

// const numberArr = [10,20,30];
// let summ = 0;
// numberArr.forEach(number=>summ+=number);
// console.log(summ);



